from django.shortcuts import render
import pyrebase
from pyrebase.pyrebase import Database

config = {

    "apiKey": "AIzaSyAfj2LjbtDQbRYKbEyyuyOncZqNXjgEr0E",

    "authDomain": "ournal-de-bord-promo-simplon.firebaseapp.com",

    "projectId": "ournal-de-bord-promo-simplon",

    "storageBucket": "ournal-de-bord-promo-simplon.appspot.com",

    "messagingSenderId": "137159583695",

    "appId": "1:137159583695:web:33ca64b15332b11e6694ba",

    "measurementId": "G-6KQ9RG0PXT",


    "databaseURL": "https://ournal-de-bord-promo-simplon-default-rtdb.europe-west1.firebasedatabase.app/",

  }

firebase = pyrebase.initialize_app(config)

auth = firebase.auth()
db = firebase.database()


def welcome(request):
    channel_nom = db.child('Data').child('Nom').get().val()
    channel_prenom = db.child('Data').child('Prenom').get().val()
    channel_retard = db.child('Data').child('Retard').get().val()
    channel_compterendu = db.child('Data').child('compte-rendu').get().val()

    channel_nomS = db.child('Second').child('Nom').get().val()
    channel_prenomS = db.child('Second').child('Prenom').get().val()
    channel_retardS = db.child('Second').child('Retard').get().val()
    channel_compterenduS = db.child('Second').child('compte-rendu').get().val()

    channel_nomT = db.child('Troisieme').child('Nom').get().val()
    channel_prenomT = db.child('Troisieme').child('Prenom').get().val()
    channel_retardT = db.child('Troisieme').child('Retard').get().val()
    channel_compterenduT = db.child('Troisieme').child('compte-rendu').get().val()


    return render(request, "welcome.html", {
        "channel_nom":channel_nom,
        "channel_prenom":channel_prenom,
        "channel_retard": channel_retard,
        "channel_compterendu":channel_compterendu,

        "channel_nomS":channel_nomS,
        "channel_prenomS":channel_prenomS,
        "channel_retardS": channel_retardS,
        "channel_compterenduS":channel_compterenduS,

        "channel_nomT":channel_nomT,
        "channel_prenomT":channel_prenomT,
        "channel_retardT": channel_retardT,
        "channel_compterenduT":channel_compterenduT,


    })

# def postsign(request):
#     email=request.POST.get('email')
#     passw = request.POST.get("pass")
#     try:
#         user = auth.sign_in_with_email_and_password(email,passw)
#     except:
#         message = "invalid cerediantials"
#         return render(request,"signIn.html",{"msg":message})
#     print(user)
#     return render(request, "welcome.html",{"e":email})

def postmessage(request):

    data={
        "task": request.POST.get("message")
    }

    db.child("message").push(data)

    message = FirebaseMsg()

    return render(request, "welcome.html", {"msg": message})


def FirebaseMsg():

    listeMessage = {}
    message = db.child("message").get()

    for ChaqueElement in message.each():
        listeMessage[ChaqueElement.key()] = ChaqueElement.val()

    return listeMessage
