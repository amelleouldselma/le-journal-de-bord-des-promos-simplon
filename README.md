# Le journal de bord des promos Simplon (ou suivi des apprenants)

# Installation du projet

° Cloner le repo

    git clone https://gitlab.com/amelleouldselma/le-journal-de-bord-des-promos-simplon.git
    cd demarrer-avec-python-et-firebase

° Installer Django && Pyrebase

    pip install -r requirements.txt

    python3 manage.py migrate
    python3 manage.py createsuperuser
